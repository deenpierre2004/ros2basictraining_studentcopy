# Introduction to ROS 2 Practical 9B / ROS 2 実習9Bへの導入

## Learning outcomes / 学習成果

This exercise will test learners on their understanding of ROS 2 Publishers, and using Service Client information / この演習では、学習者がROS 2パブリッシャーとサービスクライアント情報の使用について理解しているかをテストします。

## Practical Objective / 実習の目的

In this exercise, you will need to be able to use the pose from the ROS 2 Client in Pratical 9A to create a 3D object visualized in RViz based on the pose of the Aruco Marker detected. An example of this can be seem below. /  この演習では、実習9AのROS 2クライアントからの姿勢を使用して、検出されたArucoマーカーの姿勢に基づいてRVizで視覚化される3Dオブジェクトを作成できるようになる必要があります。これの例は以下に見ることができます。


<img src="./images/solutions.gif"  width="50%" height="50%">
<br>

## Technical requirements

1. Launch RViz 2 by running the following on a new, sourced terminal / 新しい、ソース化されたターミナルで以下を実行してRViz 2を起動します
```bash
	rviz2
```
2. Ensure that you are editing the same `object_spawner.py` file in Pratical 9A / 実習9Aの`object_spawner.py` ファイルと同じものを編集していることを確認します

3. Create a ROS 2 Publisher to publish a marker to RViz / RVizにマーカーを公開するROS 2パブリッシャーを作成します
	- Here is a **HINT** to help you. The RViz subscribes to the `/visualization_marker` topic. Make sure that `RViz2` is running, and Check what the type of this topic is and use it to create your Publisher / ここにヒントがあります。RVizは/`visualization_marker`トピックを購読します。`RViz2` が実行中であることを確認し、このトピックのタイプが何であるかを確認し、それを使用してパブリッシャーを作成します

4. Create a function and name it `publish_marker`. This function will be publishing the `cube` to RViz / `publish_marker`という名前の関数を作成します。この関数は`cube`をRVizに公開します
	- Populate a Message of the correct type to publish to rviz2. Use these values below for certain fields / rviz2に公開するための正しいタイプのメッセージを作成します。以下のフィールドには以下の値を使用します

		- `marker.header.frame_id = "/map"`
        - `marker.header.stamp = self.get_clock().now().to_msg()`
		- For other fields like color and size, you can choose what you like. / 色やサイズなどの他のフィールドについては、好きなものを選択できます。

## Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソースにすることを忘れないでください

To check if your solution works, run the following / 解決策が機能するかどうかを確認するには、以下を実行します。

### 1. Run the camera driver on the turtlebot3 / urtlebot3上でカメラドライバーを実行する

Before running the camera driver, ensure that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** / カメラドライバーを実行する前に、あなたがTURTLEBOTと同じネットワークに接続し、環境にROS_DOMAIN_IDをエクスポートしたことを確認してください。

```bash
ssh ubuntu@<ip_address>

ros2 launch camera_calibration_pkg camera_calibration.launch.py 
```

### 2. Run your image viewer node / あなたの画像ビューワーノードを実行します

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROSディストリビューションとワークスペースをソースにし、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認した上で、次のコマンドを実行してください:

```bash
ros2 run camera_service aruco_detection_node
```
### 3. Run your newly created service client / 新しく作成したサービスクライアントを実行します

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROSディストリビューションとワークスペースをソースにし、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認した上で、次のコマンドを実行してください：

```bash
ros2 run object_spawner object_spawner_node
```

### 4. Run RViz. you should see a windowed GUI client appear with an empty world /  RVizを実行します。空の世界を持つウィンドウ化されたGUIクライアントが表示されるはずです

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROSディストリビューションとワークスペースをソースにし、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認した上で、次のコマンドを実行してください：

```bash
rviz2

```

#### Click on the `Add` button at the bottom left corner to add the visualization / 左下の角にある`Add`ボタンをクリックして、視覚化を追加してください

<img src="./images/empty_rviz.png"  width="30%" height="30%">
<br>

#### Select the visualization `By topic`, `/visualization_marker` / 視覚化を`By topic`で選択し、/`visualization_marker`を選択します
<img src="./images/add_plugin.png"  width="30%" height="30%">
<br>

#### You should now see the cube in the scene / これで、シーンにキューブが表示されるはずです
<img src="./images/completed_rviz.png"  width="30%" height="30%">
<br>


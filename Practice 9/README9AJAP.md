# Introduction to ROS 2 Practical 9A / ROS 2 実習9Aへの導入

## Learning outcomes / 学習成果

This practice will test learners on their understanding of ROS 2 Service Clients. / この実習では、学習者がROS 2サービスクライアントについて理解しているかをテストします

## Exercise Objective / 実習の目的

In this practice, you will need to be able to create a package with a ROS 2 service client that will call the service server created in Practical 8B to print out the pose of an Aruco marker. / この実習では、Practical 8Bで作成したサービスサーバーを呼び出してArucoマーカーの姿勢を出力するROS 2サービスクライアントを含むパッケージを作成できるようになる必要があります

## Technical requirements / 技術要件

1. Create a ROS 2 package named `object_spawner` / `object_spawner`という名前のROS 2パッケージを作成してください

2. Create a ROS 2  executable in this package named `object_spawner_node` / このパッケージに`object_spawner_node`という名前のROS 2実行可能ファイルを作成してください

    - You may use the provided `object_spawner.py` as a starting point for your code. This file contains the `ObjectSpawnerNode` class to be the Node based object where you will create your client. / 提供された`object_spawner.py`をコードの出発点として使用することができます。このファイルには、クライアントを作成するためのノードベースのオブジェクトである`ObjectSpawnerNode`クラスが含まれています。

    - Navigate to the `training_ws/src/object_spawner/object_spawner` directory and store the `object_spawner` file there. / `training_ws/src/object_spawner/object_spawner`ディレクトリに移動し、`object_spawner`ファイルをそこに保存してください
    
3. Create a ROS 2 Service **Client** to retrieve the **pose** of the Aruco Marker detected. / 検出されたArucoマーカーの姿勢を取得するためのROS 2サービスクライアントを作成してください

4. Fill up the `send_request` function with the appropriate codes so that the client calls is able to retrieve the pose from the server and print out the pose of the Arucuo tags / クライアントがサーバーから姿勢を取得し、Arucuoタグの姿勢を出力できるように、適切なコードで`send_request`関数を埋めてください。

## Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソースにすることを忘れないでください

To check if your solution works, run the following / 解決策が機能するかどうかを確認するには、以下を実行してください

### 1. Run the camera driver on the turtlebot3 / turtlebot3上でカメラドライバーを実行する

Before running the camera driver, ensure that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** / カメラドライバーを実行する前に、あなたがTURTLEBOTと同じネットワークに接続し、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認してください

```bash
ssh ubuntu@<ip_address>

ros2 launch camera_calibration_pkg camera_calibration.launch.py 

```

### 2. Run your image viewer node / 画像ビューアノードを実行します

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROSディストリビューションとワークスペースをソースにし、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認した上で、次のコマンドを実行してください:

```bash
ros2 run camera_service aruco_detection_node

```

### 3. Run your newly created service client / 新しく作成したサービス クライアントを実行してください

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROSディストリビューションとワークスペースをソースにし、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認した上で、次のコマンドを実行してください:

```bash
ros2 run object_spawner object_spawner_node
```
You should now see a printed pose representing the location of the Aruco tag / Aruco タグの位置を表す印刷されたポーズが表示されます。


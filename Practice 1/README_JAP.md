# Introduction to Practice 1: Create a ROS 2 Package / 実践1への導入：ROS 2パッケージの作成

## Learning Outcomes / 学習成果
In this practice, users will be guided through the process of creating ROS2 packages. / この実践では、ユーザーがROS2パッケージを作成するプロセスをガイドします。

## Practice Objective / 実践目標

The user will be able to create their own ROS2 package upon completion of this practice. / この実践を完了すると、ユーザーは自分自身のROS2パッケージを作成できるようになります

## Creating a workspace for storing ROS 2 packages / ROS 2パッケージを保存するためのワークスペースを作成する

- In your Linux terminal , enter the following command / あなたのLinuxターミナルで、次のコマンドを入力してください
```linux
    mkdir training
    cd training
    mkdir new_workspace
    cd new_workspace
    mkdir src
    cd src
```

**Explanation / 説明**
1. `mkdir training`: This command creates a folder called training. / このコマンドは、trainingという名前のフォルダを作成します。

2. `cd training`: This command enters the `training` folder. / このコマンドは、`training` フォルダに移動します。

3. `mkdir new_workspace`: This command creates a folder called `new_workspace` within the `training` folder (This is your ROS 2 workspace). / このコマンドは、`training` フォルダ内に`new_workspace` というフォルダを作成します（これがあなたのROS 2ワークスペースです. 

4. `cd new_workspace` : This command enters the `new_workspace` folder / このコマンドは、`new_workspace`フォルダに移動します

5. `mkdir src`: This command creates an `src` folder within the `new_workspace folder`. This is used to store the ROS 2 packages. / このコマンドは、`new_workspace` フォルダ内に `src` フォルダを作成します。これはROS 2パッケージを保存するために使用されます。
   - Please remember that the ROS 2 packages must always be located in the src folder of the workspace. / ワークスペースのsrcフォルダには常にROS 2パッケージが配置されていることを覚えておいてください。

6. `cd src` : This command enters the `src` folder. / このコマンドは、srcフォルダに移動します。



## Creating ROS 2 Packages / ROS 2パッケージの作成

**Creating ROS 2 Python Package / Python用のROS 2パッケージを作成する**

1. In your Linux terminal, enter the following command / あなたのLinuxターミナルで、次のコマンドを入力してください

```linux 
   ros2 pkg create --build-type ament_python my_python_package
```

**Explanation**
1. `ros2 pkg create`: By using this command, ROS 2 will create a new package. / このコマンドを使用すると、ROS 2は新しいパッケージを作成してください。
2. `--build-type ament_python`: The purpose of this part is to specify the type of package to be created. In this case, it’s a Python package. / この部分の目的は、作成するパッケージのタイプを指定することです。この場合、それはPythonパッケージです。
3. `my_python_package`: Name given to package / パッケージに与えられた名前

**Inside ROS 2 Python Package / ROS 2 Pythonパッケージの中**

<img src="./Image/cmakepackage.png"  width="50%" height="50%">  
<br> 


**Creating ROS 2 cmake Package / ROS 2 cmakeパッケージの作成**

1. In your Linux terminal, enter the following command / あなたのLinuxターミナルで、次のコマンドを入力してください

```linux 
   ros2 pkg create --build-type ament_cmake my_cmake_package
```

**Explanation**
1. `ros2 pkg create`: By using this command, ROS2 will create a new package. / このコマンドを使用すると、ROS2は新しいパッケージを作成します。
2. `--build-type ament_cmake`: The purpose of this part is to specify the type of package to be created. In this case, it’s a cmake package. / この部分の目的は、作成するパッケージのタイプを指定することです。この場合、それはcmakeパッケージです。
3. `my_cmake_package`: Name given to package. / パッケージに与えられた名前

**Inside ROS 2 Cmake Package / ROS 2 Cmakeパッケージの中**

<img src="./Image/pythonpackage.png"  width="50%" height="50%">  
<br> 

# Challenge / チャレンジ

1. Create your own cmake package with the following added dependecies / 次の追加の依存関係を持つ自分自身のcmakeパッケージを作成してください。
    - `rclpp`
    - `geometry_msgs`

**Hint / ヒント:** In your Linux terminal, enter the following command /  あなたのLinuxターミナルで、次のコマンドを入力してください

```linux
    ros2 pkg create --build-type ament_cmake <name of your package> -h
```

2. Create your own python packagage with the following added dependecies / 次の追加の依存関係を持つ自分自身のPythonパッケージを作成してください。
    - `rclpy` 
    - `geometry_msgs`

**Hint / ヒント:** In your Linux terminal, enter the following command / あなたのLinuxターミナルで、次のコマンドを入力してください

```linux
    ros2 pkg create --build-type ament_python <name of your package> -h
```











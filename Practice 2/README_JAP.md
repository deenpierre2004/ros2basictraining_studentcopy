
 # Introduction to Practice 2: Create a ROS 2 Node using command line / 実践2への導入：コマンドラインを使用してROS 2ノードを作成する

## Learning outcomes
In this practice, users will be guided through the process of creating a ROS 2 Node. / この実践では、ユーザーがROS 2ノードを作成するプロセスをガイドします。

## Practice Objective

The user will be able to create their own excutable ROS 2 Node upon completion of this practice. / この実践を完了すると、ユーザーは自分自身の実行可能なROS 2ノードを作成できるようになります。


## Creating ROS 2 Python Node / ROS 2 Python ノードを作成する

### Prerequisite / 前提条件

**Make sure that you have completed these steps in the Linux terminal already / すでにLinuxターミナルでこれらのステップを完了していることを確認してください**

-  Please ensure that you have created the `my_python_package` file in the previous pratical. / 前の実習で `my_python_package` ファイルを作成したことを確認してください

**Enter the following command in linux / 次のコマンドをLinuxに入力してください**
```linux
    cd
    cd training/new_workspace/src/my_python_package/my_python_package
```

**Step 1: Create `my_node.py` file / `my_node.py` ファイルを作成する**

**Enter the following command in linux / 次のコマンドをLinuxに入力してください**
```linux
    code my_node.py
```
 - The following code will create a Python text file and launch it on Visual Studio Code. / 次のコードはPythonのテキストファイルを作成し、それをVisual Studio Codeで起動します
 
    - If you do not have Visual Studio Code, please install it / “Visual Studio Codeがインストールされていない場合は、インストールしてください


**Step 2 : Create ROS 2 Node in the file / ファイル内にROS 2ノードを作成する**
```python
import rclpy
from rclpy.node import Node

def main (args = None):
   rclpy.init(args=args)
   node = Node("my_example_node")
   print("Hello World!")
   rclpy.shutdown()

if __name__ == '__main__':
   main()
```
- Once you have finished coding, please remember to save the file. / コーディングが終わったら、ファイルを保存することを忘れないでください

**Explanation**
1. `import rclpy` : In this line, the rclpy library is imported, which is the ROS 2 standard Python library. / この行では、ROS2の標準Pythonライブラリであるrclpyライブラリがインポートされます

2. `from rclpy.node import Node` : In this line , the Node class is imported from the rclpy library. / この行では、rclpyライブラリからNodeクラスがインポートされます

3. `rclpy.init(args=args)` : In this line , the ROS 2 python client library is initialised. / この行では、ROS2のPythonクライアントライブラリが初期化されます

4. `node = Node("my_example_node")` : In this line, a ROS 2 node with the name ‘my_example_node’ is created / この行では、'my_example_node’という名前のROS2ノードが作成されます

5. `rclpy.shutdown()` : In this line, the ROS 2 system is shutdown. / この行では、ROS2システムがシャットダウンします. 


## Creating ROS 2 C++ Node / ROS 2 C++ ノードを作成する

### Prerequisite / 前提条件

**Make sure that you have completed these steps in the Linux terminal already / すでにLinuxターミナルでこれらのステップを完了していることを確認してください**

1. Please ensure that you have created the `my_cmake_package` file in the previous pratical. / 前の実習で `my_cmake_package` ファイルを作成したことを確認してください

**Enter the following command in linux / 次のコマンドをLinuxに入力してください**
```linux
    cd
    cd training/new_workspace/src/my_cmake_package/src
```

**Step 1: Create `my_node.py` file / `my_node.py` ファイルを作成する**

**Enter the following command in linux / 次のコマンドをLinuxに入力してください**

```linux
   code my_node.cpp
```
- The following code will create a C++ text file and launch it on Visual Studio Code. / 次のコードはC++のテキストファイルを作成し、それをVisual Studio Codeで起動します


**Step 2 : Create ROS 2 Node in the file / ファイル内にROS2ノードを作成する**

``` C++
#include "rclcpp/rclcpp.hpp"

int main (int argc, char * argv []){

rclcpp::init(argc,argv);

auto node = std::make_shared<rclcpp::Node>("my_node");

rclcpp::spin(node);

rclcpp::shutdown();

return 0;

}
```
- Once you have finished coding, please remember to save the file. / コーディングが終わったら、ファイルを保存することを忘れないでください

**Explanation**
1. `#include "rclcpp/rclcpp.hpp"` : In this line, the rclpp library is imported, which is the ROS 2 standard C++ library. / この行では、ROS 2の標準C++ライブラリであるrclppライブラリがインポートされます

2. `rclcpp::init(argc,argv);` : In this line , the ROS 2 system is start up. / この行では、ROS 2システムが起動します
3. `auto node = std::make_shared<rclcpp::Node>("my example node");` : In this line a ROS 2 node with the name "my example node" is created. / この行では、'my example node’という名前のROS 2ノードが作成されます
4. `rclcpp::spin(node);` : In this line the ROS 2 node is spin (running). / この行では、ROS 2ノードがスピン（実行）されます
5. `rclcpp::shutdown();` : In this line the ROS 2 system is shutdown. / この行では、ROS 2システムがシャットダウンします


# Practice 2B: Creating an executable ROS 2 Node (Python) / 実行可能な ROS 2 ノードの作成 (Python)

### Prerequisite / 前提条件
- Please ensure that the Python script for the Node has been created. / ノードの Python スクリプトが作成されていることを確認してください。

**Step 1: Enter the setup.py file in your my_python_package directionary / `my_python_package` ディレクトリの setup.py ファイルを入力してください。**

<img src="./Image/pythonpackage.png"  width="50%" height="50%">  
<br> 

**Step 2 : Creating instructions to built executable / 実行可能ファイルを作成するための指示を作成する**

The following line should be added within the console_scripts brackets of the entry_points field / 次の行は、entry_pointsフィールドのconsole_scriptsブラケット内に追加する必要があります:

``` python
 'my_node_executable = my_python_package.my_node:main'
 ```
**Explanation**
1. `my_node_executable` : This is the executable name. / これは実行可能な名前です。
2. `my_python_package` : This is the folder containg the python node script. / これはPythonノードスクリプトを含むフォルダです。
3. `my_node` :  Name of the python script. / Pythonスクリプトの名前。
4. `main`: Name of the function. / 関数の名前。

**Step 3 : Building Package**

Enter the following command in your linux terminal / 次のコマンドをLinuxのターミナルに入力します。
```linux
cd
cd training/new_workspace
colcon build --packages-select <package name>
```
-  `colcon build --packages-select <package name>` : This command line is used to build ROS 2 packages. / このコマンドラインは、ROS 2パッケージをビルドするために使用されます。

**Step 4 : Sourcing script**

- Open a new terminal / 新しいターミナルを開きます。
- Enter the following command in your linux terminal / 次のコマンドをLinuxのターミナルに入力します
```linux
cd
cd training/new_workspace
source install/setup.bash
```
- `source install/setup.bash` : This command line is used to source the script / このコマンドラインは、スクリプトをソースするために使用されます。
  - In order to use packages in your workspace, you need to source the workspace's setup script. / ワークスペースのパッケージを使用するためには、ワークスペースのセットアップスクリプトをソースする必要があります

**Step 5 : Running Executable / 実行可能ファイルを実行する**
```linux
ros2 run <package name> <executable name>
```

## Challenge / チャレンジ
1. Create your own ROS 2 Node with an executable package using C++. / C++を使用して実行可能パッケージを持つ自分だけのROS2ノードを作成します。
   - Create a message in the ROS 2 node which will be printed in the terminal. / ターミナルに表示されるROS 2ノードにメッセージを作成します。
 






# Introduction to Practice 6: ROS 2 Publisher with complex message / 複雑なメッセージを含む ROS 2 Publisher

## Learning outcomes /  学習成果
In this practice it will test learners on their understanding of ROS 2 Publisher with complex message. / この実習では、複雑なメッセージを持つROS 2 Publisherについての学習者の理解をテストします

## Practice Objective / 実習目標

The user will be able to create their own ROS 2 Publisher with complex message upon completion of this practice. / この実習を完了すると、ユーザーは自分自身の複雑なメッセージを持つROS 2 Publisherを作成することができます。

## Step 1 : Creating a custom message interface / カスタムメッセージインターフェースの作成

**Technical Requirements / 技術要件**

1. Within the same `training_ws` where you have done your previous pratical at, go into the src folder / 以前の実習を行った同じ`training_ws`内に移動し、srcフォルダに入ってください

2. Create a ROS 2 interface package named `simple_velocity_msg` within the src folder / srcフォルダ内に`simple_velocity_msg`という名前のROS 2インターフェースパッケージを作成してください

3. Inside the `simple_velocity_msg` package, create a file named `msg` / `simple_velocity_msg`パッケージ内に`msg`という名前のファイルを作成してください

4. Inside the `msg` file , create a message interface named `SimpleVelocity.msg` / `msg`ファイル内に`SimpleVelocity.msg`という名前のメッセージインターフェースを作成してください

    - Remember that your interface file should be named in CamelCase, as it is part of the ROS 2 guidelines / インターフェースファイルは、ROS 2のガイドラインの一部であるため、CamelCaseで命名する必要があります。

    - Your interface file must contain two float64 type file named linear_velocity and angular_velocity respectively / インターフェースファイルには、それぞれlinear_velocityとangular_velocityという名前のfloat64型のファイルが含まれている必要があります

5. Inside your CMakelist.txt, include the neccessary dependecies below / CMakelist.txt 内に、以下の必要な依存関係を含めます `# find_package(<dependency> REQUIRED)`
```
find_package(rosidl_default_generators REQUIRED)
find_package(geometry_msgs REQUIRED)
rosidl_generate_interfaces(${PROJECT_NAME}
    "msg/SimpleVelocity.msg"
  DEPENDENCIES geometry_msgs
)
```

6. Inside your `package.xml` file , include the following dependecies within the `<package>` element of `package.xml` file. / あなたの `package.xml` ファイルの中に、以下の依存関係を `package.xml` ファイルの <package> 要素内に含めてください

```
<depend>geometry_msgs</depend>
<buildtool_depend>rosidl_default_generators</buildtool_depend>
<exec_depend>rosidl_default_runtime</exec_depend>
<member_of_group>rosidl_interface_packages</member_of_group>

```
7. Make sure that all the files are saved prior to building the `simple_velocity_msg` package. / `simple_velocity_msg` パッケージをビルドする前に、すべてのファイルが保存されていることを確認してください

## Step 2 : Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソースにすることを忘れないでください

To check if your solution works, go into a new terminal and run the following commands / あなたの解決策が機能するかどうかを確認するために、新しいターミナルを開き、以下のコマンドを実行してください:

``` linux
cd training_Ws
source install/setup.bash
ros2 interface list
```
You should be able to see `simple_velocity_msg/msg/SimpleVelocity` as one of the interfaces under `Messages` / `Messages` のインターフェースの一つとして `simple_velocity_msg/msg/SimpleVelocity` を見ることができるはずです


## Step 3 : Creating ROS 2 Publisher / ROS 2 パブリッシャーの作成

**Technical Requirements / 技術要件**

1. Within the `training_ws` folder , go inside the src folder / `training_ws` フォルダ内で、src フォルダに入ってください

2. Create a ROS 2 python package named `complex_publisher` within the src folder / src フォルダ内に `complex_publisher` という名前の ROS 2 python パッケージを作成してください

3. Create a ROS 2 excutable in this package named `practical6_node` / このパッケージに `practical6_node` という名前の ROS 2 実行可能ファイルを作成してください

4. You may use the provided code boiler plate `my_publisher.py` as a starting point / 提供されたコードボイラープレート `my_publisher.py` を開始点として使用することができます

    - Navigate to the `training_ws/src/complex_publisher/complex_publisher` directory and store the `my_publisher.py` file there / `training_ws/src/complex_publisher/complex_publisher` ディレクトリに移動し、そこに `my_publisher.py` ファイルを保存してください
   
5. Inside the `my_publisher file`, implement the following task / `my_publisher` file の中で、以下のタスクを実装してください.

    1. Import the SimpleVelocity Message. / SimpleVelocity メッセージをインポートしてください

    2. Create a publisher node that publishes the SimpleVelocity messages to the `simple_vel` topic. / `simple_vel` トピックに SimpleVelocity メッセージを公開するパブリッシャーノードを作成してください. 

    3. Create a subscriber that listens to `Twist` messages on the `cmd_vel` topic. / `cmd_vel` トピックの `Twist` メッセージをリッスンするサブスクライバを作成してください。

    4. Initialise a twist message and name it self.msg / twistメッセージを初期化し、self.msgという名前を付けてください。

    5. Create a timer and it should be set at 1 seconds / タイマーを作成し、それを 1 秒に設定してください。

    6. Implement a timer callback function that is called every second. / 毎秒呼び出されるタイマーコールバック関数を実装してください。

        - Publish the Velocity message to the simple_vel topic. / Velocityメッセージをsimple_velトピックにパブリッシュしてください

    - #### Use the comments in the provided code boiler plate as a guideline / 提供されたコードボイラープレートのコメントをガイドラインとして使用します


6. Inside your `package.xml` file, include the following dependecies within the `<package>` element of `package.xml` file / `package.xml` ファイルの中で、`package.xml` ファイルの <package> 要素内に以下の依存関係を含めてください。

```
<exec_depend>velocity_msg</exec_depend>

```

7. Make sure that all the files are saved prior to building the `complex_publisher` package.

## Step 4 : Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソースにすることを忘れないでください

### 1. Bringup and initialise the turtlebot / Turtlebotの起動と初期化

Check that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT**. / あなたが TURTLEBOTと同じネットワークに接続していることを確認してください

In a new terminal, run the following commands / 新しいターミナルで、以下のコマンドを実行してください:

```bash
ssh ubuntu@<ip_address>

ros2 launch turtlebot3_bringup robot.launch.py 

```
Once you see the following output from the terminal, it means that you have successfully bringup the turtlebot / ターミナルから以下の出力が見えたら、それはあなたが成功して turtlebot を起動したことを意味します.

```bash
[INFO] [launch]: All log files can be found below /home/ubuntu/.ros/log/2019-08-19-01-24-19-009803-ubuntu-15310
[INFO] [launch]: Default logging verbosity is set to INFO
urdf_file_name : turtlebot3_burger.urdf
[INFO] [robot_state_publisher-1]: process started with pid [15320]
[INFO] [hlds_laser_publisher-2]: process started with pid [15321]
[INFO] [turtlebot3_ros-3]: process started with pid [15322]
[robot_state_publisher-1] Initialize urdf model from file: /home/ubuntu/turtlebot_ws/install/turtlebot3_description/share/turtlebot3_description/urdf/turtlebot3_burger.urdf
[robot_state_publisher-1] Parsing robot urdf xml string.
[robot_state_publisher-1] Link base_link had 5 children
[robot_state_publisher-1] Link caster_back_link had 0 children
[robot_state_publisher-1] Link imu_link had 0 children
[robot_state_publisher-1] Link base_scan had 0 children
[robot_state_publisher-1] Link wheel_left_link had 0 children
[robot_state_publisher-1] Link wheel_right_link had 0 children
[robot_state_publisher-1] got segment base_footprint
[robot_state_publisher-1] got segment base_link
[robot_state_publisher-1] got segment base_scan
[robot_state_publisher-1] got segment caster_back_link
[robot_state_publisher-1] got segment imu_link
[robot_state_publisher-1] got segment wheel_left_link
[robot_state_publisher-1] got segment wheel_right_link
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Init TurtleBot3 Node Main
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Init DynamixelSDKWrapper
[turtlebot3_ros-3] [INFO] [DynamixelSDKWrapper]: Succeeded to open the port(/dev/ttyACM0)!
[turtlebot3_ros-3] [INFO] [DynamixelSDKWrapper]: Succeeded to change the baudrate!
[robot_state_publisher-1] Adding fixed segment from base_footprint to base_link
[robot_state_publisher-1] Adding fixed segment from base_link to caster_back_link
[robot_state_publisher-1] Adding fixed segment from base_link to imu_link
[robot_state_publisher-1] Adding fixed segment from base_link to base_scan
[robot_state_publisher-1] Adding moving segment from base_link to wheel_left_link
[robot_state_publisher-1] Adding moving segment from base_link to wheel_right_link
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Start Calibration of Gyro
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Calibration End
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Motors
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Wheels
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Sensors
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create battery state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create imu publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create sensor state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create joint state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Devices
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create motor power server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create reset server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create sound server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Run!
[turtlebot3_ros-3] [INFO] [diff_drive_controller]: Init Odometry
[turtlebot3_ros-3] [INFO] [diff_drive_controller]: Run!
```

### 2. To check if your solution works, go into a new terminal and run the following commands / ソリューションが機能するかどうかを確認するには、新しいターミナルに移動して次のコマンドを実行します:

``` linux
cd training_ws
source install/setup.bash
ros2 run complex_publisher pratical6_node
```

Once you see the following output from the terminal, it means that you have successfully completed this practical / ターミナルから以下の出力が見えたら、それはあなたが成功して turtlebot を起動したことを意味します

```
[INFO] [1714114534.937899659] [MyPublisher]: Publishing linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714114535.931171274] [MyPublisher]: Publishing linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714114536.931050675] [MyPublisher]: Publishing linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714114537.931097800] [MyPublisher]: Publishing linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714114538.931091920] [MyPublisher]: Publishing linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
```

# Introduction to Practice 4: Create your own Subscriber Node / 自分自身のサブスクライバーノードを作成します

## Learning outcomes / 学習成果

This exercise will test learners on their understanding of Subscribers. / この演習では、サブスクライバーについての理解を試します

## Exercise Objective / 演習の目的

In this exercise, you will need to be able to create a package that allows you to display an image from a usb webcam. / この演習では、USBウェブカメラから画像を表示できるパッケージを作成する能力が必要です

You will be using a **ROS 2 Subscriber** to subscribe to the image published by the camera driver to the topic named `/image_raw` / “ROS2 Subscriberを使用して、カメラドライバーによって公開された画像を`/image_raw` というトピックに登録します

## Technical requirements / 技術的要件

1. Within the same `training_ws` created in Practice 3, go into the src folder / Practice 3で作成した `training_ws` 内にあるsrcフォルダに移動してください

2. Create a ROS 2 package named `image_viewer` within the `src` folder / `src` フォルダ内に`image_viewer` という名前のROS 2パッケージを作成してください

3. Create a ROS 2 executable within this package named `view_image` / このパッケージ内に`view_image`という名前のROS 2実行可能ファイルを作成しぇてください

    - You may use the provided code boiler plate `view_image.py` as a starting point / 提供されたコードボイラープレート`view_image.py`を出発点として使用することができます
    
    - Navigate to the `training_ws/src/view_image/view_image` directory and store the `teleop.py` file there. / `training_ws/src/view_image/view_image` ディレクトリに移動し、そこに`teleop.py`ファイルを保存してください。

4. Create a node that is able to subscribe to the `/image_raw` topic / `/image_raw`トピックを購読できるノードを作成してください

5. To Display your image, you may use the following helper function / 画像を表示するために、次のヘルパー関数を使用することができます:

```python
def view_image(img_msg)
    cv_image = self.bridge.imgmsg_to_cv2(img_msg, desired_encoding='passthrough')
    cv2.imshow("image", cv_image)
    cv2.waitKey(1)
```

## Verifying your solution / あなたの解決策を確認します

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドし、ソースにすることを忘れないでください

To check if your solution works, run the following / あなたの解決策が機能するかどうかを確認するために、次の操作を実行してください

### 1. Run the camera driver on the turtlebot3 / Turtlebot3でカメラドライバーを実行します

Before running the camera driver, ensure that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** / カメラドライバーを実行する前に、あなたがTURTLEBOTと同じネットワークに接続していて、環境内で`ROS_DOMAIN_ID`をエクスポートしたことを確認してください

```bash
ssh ubuntu@<ip_address>

ros2 launch usb_cam demo_launch.py

```

<!-- before running the driver, we need to check what interfaces are available. Do the following:

```bash
v4l2-ctl --list-devices

```

you should be able to see the list of devices below (It may not be exactly the same):

```bash

Integrated_Webcam_HD: Integrate (usb-0000:00:14.0-11):
	/dev/video0
	/dev/video1
	/dev/video2
	/dev/video3
	/dev/media0
	/dev/media1

UVC Camera (046d:0825) (usb-0000:00:14.0-2):
	/dev/video4
	/dev/video5
	/dev/media2


We want to use the **USB** camera, which, in this example above, would be the device `/dev/video4` OR `/dev/video5`. To figure out which one it is we will need to do trial and error. In the `img.yaml` config file in the `config` folder of the `camera_driver` package, change the **video_device** paramter to the device id you will want to test. After which, run the following command.

```bash
ros2 launch camera_driver cam.launch.py
```

if successful, your terminal should output this below, if not, switch to the other device ID and test it. 
```bash
[usb_cam_node_exe-1] [INFO] [] [camera]: camera_name value: narrow_stereo
[usb_cam_node_exe-1] [WARN] [] [camera]: framerate: 10.000000
[usb_cam_node_exe-1] [INFO] [] [camera]: camera calibration URL: package://camera_driver/config/img.yaml
[usb_cam_node_exe-1] [INFO] [] [camera]: Starting 'narrow_stereo' (/dev/video4) at 640x480 via mmap (yuyv) at 10 FPS
[usb_cam_node_exe-1] [INFO] [] [camera]: This devices supproted formats:
[usb_cam_node_exe-1] [INFO] [] [camera]: 	YUYV 4:2:2: 640 x 480 (30 Hz)
[usb_cam_node_exe-1] [INFO] [] [camera]: 	Motion-JPEG: 1280 x 960 (10 Hz)
[usb_cam_node_exe-1] [INFO] [] [camera]: 	Motion-JPEG: 1280 x 960 (5 Hz)
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'brightness' to 50
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'white_balance_temperature_auto' to 1
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'exposure_auto' to 3
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'focus_auto' to 0
[usb_cam_node_exe-1] [INFO] [] [camera]: Timer triggering every 100 ms

``` -->


### 2. Run your image viewer node / あなたの画像ビューワーノードを実行します
In a new terminal within your VM, ensure that you have **EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** / VM内の新しいターミナルで、環境内で`ROS_DOMAIN_ID`をエクスポートしたことを確認してください

Then run the following command for the image viewer node / 次に、画像ビューワーノードのための次のコマンドを実行します :

```bash
ros2 run image_viewer view_image
```

## Hints / ヒント

1. First run the camera driver. What is the message type for this topic? / まず、カメラドライバーを実行します。このトピックのメッセージタイプは何ですか？

2. What is the structure of this ROS 2 interface? / このROS2インターフェースの構造は何ですか？


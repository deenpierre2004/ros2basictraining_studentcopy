# Introduction to Practical 8A : Creating ROS 2 custom interface / ROS 2カスタムインターフェースの作成

## Learning outcomes / 学習成果

This exercise will test learners on their understanding of Custom Interfaces / この演習では、カスタムインターフェースについての理解を学習者に試します

## Exercise Objective / 演習の目的

In this exercise, you will need to be able to create a custom interface package / この演習では、カスタム インターフェイス パッケージを作成できる必要があります。

## Technical requirements / 技術要件

1. Create an interface package called `perception_msgs` / `perception_msgs` というインターフェースパッケージを作成してください。

2. In the `perception_msgs` package, Add a service interface GetObjectLocation.srv that contains /`perception_msgs` パッケージに、以下を含むサービスインターフェースGetObjectLocation.srvを追加してください:
    - An empty service **request** field / 空のサービスリクエストフィールド

    - A service **result** that contains / 結果という名前のサービス結果フィールドが含まれています:
        a **bool** type field named `result` / `result`という名前の **bool** 型フィールド、,
        a **Pose** type field named `object_pose` / `object_pose`という名前の **Pose** 型フィールド

## Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソースにすることを忘れないでください

To check if your solution works, run the following / ソリューションが機能するかどうかを確認するには、次のコマンドを実行してください。

### 1. Run ROS2 interface list

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROSディストリビューションとワークスペースをソースにし、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認した上で、次のコマンドを実行してください:


```bash
ros2 interface list

```

You should be able to see `perception_msgs/srv/GetObjectLocation` as one of the interfaces under `Services` / `Services`のインターフェースの一つとして`perception_msgs/srv/GetObjectLocation`が表示されるはずです


# Introduction to Practical 8B : Creating a ROS 2 Service Server / ROS 2 サービスサーバーの作成
## Learning Outcomes / 学習成果

This pratical will test learners on their understanding of ROS 2 Service Servers. / この実習では、学習者がROS 2サービスサーバーについて理解しているかをテストします。 

<!-- ## Exercise Installation

Before starting the exercise, you would need to do the following

<WIP, include the installation> -->

## Practical Objective / 実習の目的

In this exercise, you will need to be able to create a package that creates a ROS 2 service server that allows users to request the position of a detected object viewed by the camera. / この演習では、ユーザーがカメラで見た検出されたオブジェクトの位置を要求できるようにするROS 2サービスサーバーを作成するパッケージを作成できるようになる必要があります。

<img src="./images/object_detection.png"  width="80%" height="80%">
<br>


You will be creating a **ROS 2 Service Server** to provide service client access to an object detection function. / あなたは、サービスクライアントがオブジェクト検出機能にアクセスできるようにするROS 2サービスサーバーを作成します。

## Technical requirements / 技術要件
1. Create a ROS 2 package named `camera_service` / `camera_service`という名前のROS 2パッケージを作成します。

2. Create a ROS 2 executable in this package named `aruco_detection_node` / このパッケージにaruco_detection_nodeという名前のROS 2実行可能ファイルを作成してください。

    You may use the provided `aruco_detection_node.py` as a starting point for your code. This file contains： / 提供された `aruco_detection_node.py` をコードの出発点として使用することができます。このファイルには以下が含まれています：
    1. The `ArucoDetection` class to be used for image detection. / 画像検出に使用される`ArucoDetection`クラス
    2. A subscriber that processes and stores the object coordinates detected by the perception system / パーセプションシステムによって検出されたオブジェクトの座標を処理し保存するサブスクライバー

3. Navigate to the `training_ws/src/camera_service/camera_service` directory and store the `aruco_detection_node.py` file there. / `training_ws/src/camera_service/camera_service`ディレクトリに移動し、`aruco_detection_node.py`ファイルをそこに保存してください

    - Inside the `aruco_detection_node.py` file . Do the following / `aruco_detection_node.py` ファイルの中で以下を行ってください

        - Create a ROS 2 Service **Server** named `find_objects` that returns the stored coordinates to the client requesting for this service. / クライアントがこのサービスを要求したときに保存された座標を返す `find_objects` という名前のROS 2サービスサーバーを作成してください。

        - Note that in this exercise, you will need to add a **callback group** to your service. / この演習では、サービスにコールバックグループを追加する必要があります

        - Dont worry! for this exercise all you need to do is to add another parameter `callback_group = server_cb_group` as the last parameter when calling the `create_service` function / 心配しないでください！この演習では、`create_service` 関数を呼び出すときに最後のパラメータとして `callback_group = server_cb_group` を追加するだけで大丈夫です。

        - Use the GetObjectLocation service type as the type for this Server. / このサーバーのタイプとしてGetObjectLocationサービスタイプを使用します

4. Navigate to the `training_ws/src/camera_service/camera_service` directory and store the provided `transformations.py` file there. / `training_ws/src/camera_service/camera_service` ディレクトリに移動し、提供された `transformations.py` ファイルをそこに保存します。

## Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソースにすることを忘れないでください

To check if your solution works, run the following / 解決策が機能するかどうかを確認するには、以下を実行します。

### 1. Run the camera driver on the turtlebot3 / turtlebot3上でカメラドライバーを実行する

Before running the camera driver, ensure that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** / カメラドライバーを実行する前に、あなたがTURTLEBOTと同じネットワークに接続し、環境に`ROS_DOMAIN_ID`をエクスポートしたことを確認してください

```bash
ssh ubuntu@<ip_address>

ros2 launch usb_cam demo_launch.py

```

<!-- 
before running the driver, we need to check what interfaces are available. Do the following:

```bash
v4l2-ctl --list-devices

```

you should be able to see the list of devices below (It may not be exactly the same):

```bash

Integrated_Webcam_HD: Integrate (usb-0000:00:14.0-11):
	/dev/video0
	/dev/video1
	/dev/video2
	/dev/video3
	/dev/media0
	/dev/media1

UVC Camera (046d:0825) (usb-0000:00:14.0-2):
	/dev/video4
	/dev/video5
	/dev/media2
```

We want to use the **USB** camera, which would be the device `/dev/video4` OR `/dev/video5`. To figure out which one it is we will need to trial and error. In the `img.yaml` config file in the `config` folder of the `camera_driver` package, change the **video_device** paramter to the device id you will want to test. After which, run the following command.

```bash
ros2 launch camera_driver cam.launch.py
```

if successful, your terminal should output this below, if not, switch to the other device ID and test it. 
```bash
[usb_cam_node_exe-1] [INFO] [] [camera]: camera_name value: narrow_stereo
[usb_cam_node_exe-1] [WARN] [] [camera]: framerate: 10.000000
[usb_cam_node_exe-1] [INFO] [] [camera]: camera calibration URL: package://camera_driver/config/img.yaml
[usb_cam_node_exe-1] [INFO] [] [camera]: Starting 'narrow_stereo' (/dev/video4) at 640x480 via mmap (yuyv) at 10 FPS
[usb_cam_node_exe-1] [INFO] [] [camera]: This devices supproted formats:
[usb_cam_node_exe-1] [INFO] [] [camera]: 	YUYV 4:2:2: 640 x 480 (30 Hz)
[usb_cam_node_exe-1] [INFO] [] [camera]: 	Motion-JPEG: 1280 x 960 (10 Hz)
[usb_cam_node_exe-1] [INFO] [] [camera]: 	Motion-JPEG: 1280 x 960 (5 Hz)
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'brightness' to 50
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'white_balance_temperature_auto' to 1
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'exposure_auto' to 3
[usb_cam_node_exe-1] [INFO] [] [camera]: Setting 'focus_auto' to 0
[usb_cam_node_exe-1] [INFO] [] [camera]: Timer triggering every 100 ms

``` -->

### 2. Run your image viewer node / 画像ビューアノードを実行します 

```bash
ros2 run camera_service aruco_detection_node

```
### 3. Run a service client via the command line / コマンドライン経由でサービスクライアントを実行する

```bash
ros2 service call /find_objects perception_msgs/srv/GetObjectLocation "{}"
```

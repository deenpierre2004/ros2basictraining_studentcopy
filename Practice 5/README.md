# Introduction to Practice 5: Creating a ROS 2 Node which contains a Publisher and Subscriber / パブリッシャーとサブスクライバーを含むROS 2ノードを作成します

## Learning outcomes / 学習成果
In this practice it will test learners on their understanding of ROS 2 Publisher and Subscriber / この練習では、学習者がROS 2のパブリッシャーとサブスクライバーについて理解しているかをテストします

## Practice Objective / 練習の目的
The user will be able to create their own ROS 2 Node which has both a Publisher and Subscriber / ユーザーは、パブリッシャーとサブスクライバーの両方を持つ自分自身のROS 2ノードを作成することができます

## Prerequisite / 前提条件

1. Within the `training_ws` folder , go inside the src folder / `training_ws`フォルダー内のsrcフォルダーに入ってください

2. Create two different ROS 2 python package named `numone` and `numtwo` respectively within the src folder / srcフォルダ内に`numone`と`numtwo`という名前の2つの異なるROS 2 pythonパッケージをそれぞれ作成してください。

## Task List (numone) / タスクリスト（numone）

1. Create ROS 2 executable for `numone` python package and named it `practical5_nodeone` / numone pythonパッケージのROS 2実行可能ファイルを作成し、それを`practical5_nodeone`と名付けてください

    
2. You may use the provided code boiler plate `numone.py` as a starting point / 提供されたコードボイラープレート`numone.py`を開始点として使用することができます

    - Navigate to the `training_ws/src/numone/numone` directory and store the `numone.py` file there / `training_ws/src/numone/numone`ディレクトリに移動し、そこに`numone.py`ファイルを保存してください


3. Inside the `numone.py` file, enter the neccessary codes. / `numone.py`ファイルの中に、必要なコードを入力してください

    1. Inside the node create a publisher that publishes `string` type messages to the `first_node` topic / ノード内部で、`first_node`トピックに`string` 型のメッセージを公開するパブリッシャーを作成してください

    2. After creating the subscriber , create a subscriber that subscribes to `string` type messages from the `second_node` topic / サブスクライバーを作成した後、`second_node`トピックから`string`型のメッセージを購読するサブスクライバーを作成してください

    3. Implement a timer callback function that is called every 3 seconds. /3秒ごとに呼び出されるタイマーコールバック関数を実装してください
        
        - This function should create the following output / この関数は以下の出力を作成するべきです
            - Use the comments in the provided code boiler plate as a guideline / 提供されたコードボイラープレートのコメントをガイドラインとして使用してください
        ```
        [INFO] [1714357683.067692883] [nodeone]: Publishing : "Ping : 0"
        [INFO] [1714357688.063200369] [nodeone]: Publishing : "Ping : 1"
        [INFO] [1714357693.063168647] [nodeone]: Publishing : "Ping : 2"
        [INFO] [1714357698.063105014] [nodeone]: Publishing : "Ping : 3"
        [INFO] [1714357703.063034921] [nodeone]: Publishing : "Ping : 4"
        [INFO] [1714357708.063037977] [nodeone]: Publishing : "Ping : 5"
        [INFO] [1714357713.063032697] [nodeone]: Publishing : "Ping : 6"
        [INFO] [1714357718.063035009] [nodeone]: Publishing : "Ping : 7"
        [INFO] [1714357723.063040038] [nodeone]: Publishing : "Ping : 8"
        [INFO] [1714357728.063124756] [nodeone]: Publishing : "Ping : 9"
        [INFO] [1714357733.063029461] [nodeone]: Publishing : "Ping : 10"
        ```
        
    5. Create a listener callback function / リスナーコールバック関数を作成してください
    
        - This function should dispay the following output / この関数は以下の出力を表示するべきです

            - Use the comments in the provided code boiler plate as a guideline / 提供されたコードボイラープレートのコメントをガイドラインとして使用してください

        ```
        [INFO] [1714357836.621144457] [nodeone]: Subscribing : "Pong : 1"
        ```

## Task List (numtwo) / タスクリスト（numtwo）


1. Create ROS 2 excutable for `numtwo` python package and named it `practical5_nodetwo` / `numtwo` pythonパッケージのROS 2実行可能ファイルを作成し、それを`practical5_nodetwo`と名付けてください。


2.  You may use the provided code boiler plate `numtwo.py` as a starting point / 提供されたコードボイラープレート`numtwo.py`を開始点として使用することができます

    - Navigate to the `training_ws/src/numtwo/numtwo` directory and store the `numtwo.py` file there / `training_ws/src/numtwo/numtwo`ディレクトリに移動し、そこにnumtwo.pyファイルを保存してください


3. Inside the `numtwo.py` file, enter the neccessary codes. / `numtwo.py`ファイルの中に、必要なコードを入力してください

    1. Inside the node create a publisher that publishes `string` type messages to the `second_node` topic / ノード内部で、`second_node`トピックに`string`型のメッセージを公開するパブリッシャーを作成してください

    2. After creating the subscriber , create a subscriber that subscribes to `string` type messages from the `first_node` topic / サブスクライバーを作成した後、`first_node`トピックから`string`型のメッセージを購読するサブスクライバーを作成してください

    3. Implement a timer callback function that is called every 3 seconds. / 3秒ごとに呼び出されるタイマーコールバック関数を実装してください
        
        - This function should create the following output / この関数は以下の出力を作成するべきです
            - Use the comments in the provided code boiler plate as a guideline / 提供されたコードボイラープレートのコメントをガイドラインとして使用してください
        ```
        [INFO] [1714358349.065487408] [nodetwo]: Publishing: "Pong : 0"
        [INFO] [1714358354.060928008] [nodetwo]: Publishing: "Pong : 1"
        [INFO] [1714358359.060985198] [nodetwo]: Publishing: "Pong : 2"
        [INFO] [1714358364.061010045] [nodetwo]: Publishing: "Pong : 3"
        [INFO] [1714358369.060962576] [nodetwo]: Publishing: "Pong : 4"
        [INFO] [1714358374.060856817] [nodetwo]: Publishing: "Pong : 5"
        [INFO] [1714358379.060863378] [nodetwo]: Publishing: "Pong : 6"
        [INFO] [1714358384.060848672] [nodetwo]: Publishing: "Pong : 7"
        [INFO] [1714358389.060851505] [nodetwo]: Publishing: "Pong : 8"
        [INFO] [1714358394.060856480] [nodetwo]: Publishing: "Pong : 9"
        [INFO] [1714358399.060840799] [nodetwo]: Publishing: "Pong : 10"
        [INFO] [1714358404.060855635] [nodetwo]: Publishing: "Pong : 11"
        [INFO] [1714358409.060857030] [nodetwo]: Publishing: "Pong : 12"
        [INFO] [1714358414.060844755] [nodetwo]: Publishing: "Pong : 13"

        ```
    4. Create a listener callback function / リスナーコールバック関数を作成してください
    
        - This function should dispay the following output / この関数は以下の出力を表示するべきです
            - Use the comments in the provided code boiler plate as a guideline / 提供されたコードボイラープレートのコメントをガイドラインとして使用してください
        ```
        [INFO] [1714358463.862837771] [nodetwo]: Subscribing: "Ping : 0"
        ```

### Make sure that all the files are saved prior to building the both packages. / 両方のパッケージをビルドする前に、すべてのファイルが保存されていることを確認してください

## Verifying your solution / ソリューションの検証

1. Open two new terminals / 2つの新しいターミナルを開きてください

2. In the first terminal Run the following command / 最初のターミナルで以下のコマンドを実行してください
```
ros2 run numone pratical5_nodeone

```

3. In the second terminal, run the following command / 2つ目のターミナルで、以下のコマンドを実行してください
```
ros2 run numtwo pratical5_nodetwo

```

### Correct output for terminal 1 / 端子 1 の正しい出力

```
[INFO] [1714359771.316522306] [nodeone]: Publishing : "Ping : 0"
[INFO] [1714359773.824080816] [nodeone]: Subscribing : "Pong : 0"
[INFO] [1714359776.312134331] [nodeone]: Publishing : "Ping : 1"
[INFO] [1714359778.823931483] [nodeone]: Subscribing : "Pong : 1"
[INFO] [1714359781.312129546] [nodeone]: Publishing : "Ping : 2"
[INFO] [1714359783.824183543] [nodeone]: Subscribing : "Pong : 2"
[INFO] [1714359786.312150647] [nodeone]: Publishing : "Ping : 3"
[INFO] [1714359788.824116526] [nodeone]: Subscribing : "Pong : 3"
[INFO] [1714359791.312144942] [nodeone]: Publishing : "Ping : 4"
[INFO] [1714359793.824119221] [nodeone]: Subscribing : "Pong : 4"
[INFO] [1714359796.312054592] [nodeone]: Publishing : "Ping : 5"
[INFO] [1714359798.824109099] [nodeone]: Subscribing : "Pong : 5"
[INFO] [1714359801.312044302] [nodeone]: Publishing : "Ping : 6"
[INFO] [1714359803.824200711] [nodeone]: Subscribing : "Pong : 6"
[INFO] [1714359806.312057583] [nodeone]: Publishing : "Ping : 7"
[INFO] [1714359808.824119014] [nodeone]: Subscribing : "Pong : 7"
[INFO] [1714359811.312059610] [nodeone]: Publishing : "Ping : 8"
[INFO] [1714359813.824152847] [nodeone]: Subscribing : "Pong : 8"
[INFO] [1714359816.312074652] [nodeone]: Publishing : "Ping : 9"
[INFO] [1714359818.824281212] [nodeone]: Subscribing : "Pong : 9"
[INFO] [1714359821.312061797] [nodeone]: Publishing : "Ping : 10"
[INFO] [1714359823.824299975] [nodeone]: Subscribing : "Pong : 10"
```
### Correct output for terminal 2 / ターミナル 2 の正しい出力

```
[INFO] [1714359771.316924416] [nodetwo]: Subscribing: "Ping : 0"
[INFO] [1714359773.823716592] [nodetwo]: Publishing: "Pong : 0"
[INFO] [1714359776.312475252] [nodetwo]: Subscribing: "Ping : 1"
[INFO] [1714359778.823617381] [nodetwo]: Publishing: "Pong : 1"
[INFO] [1714359781.312469043] [nodetwo]: Subscribing: "Ping : 2"
[INFO] [1714359783.823824407] [nodetwo]: Publishing: "Pong : 2"
[INFO] [1714359786.312414526] [nodetwo]: Subscribing: "Ping : 3"
[INFO] [1714359788.823810879] [nodetwo]: Publishing: "Pong : 3"
[INFO] [1714359791.312444505] [nodetwo]: Subscribing: "Ping : 4"
[INFO] [1714359793.823840633] [nodetwo]: Publishing: "Pong : 4"
[INFO] [1714359796.312370097] [nodetwo]: Subscribing: "Ping : 5"
[INFO] [1714359798.823805104] [nodetwo]: Publishing: "Pong : 5"
[INFO] [1714359801.312392819] [nodetwo]: Subscribing: "Ping : 6"
[INFO] [1714359803.823866708] [nodetwo]: Publishing: "Pong : 6"
[INFO] [1714359806.312233615] [nodetwo]: Subscribing: "Ping : 7"
[INFO] [1714359808.823768224] [nodetwo]: Publishing: "Pong : 7"
[INFO] [1714359811.312365798] [nodetwo]: Subscribing: "Ping : 8"
[INFO] [1714359813.823758150] [nodetwo]: Publishing: "Pong : 8"
[INFO] [1714359816.312448220] [nodetwo]: Subscribing: "Ping : 9"
[INFO] [1714359818.823883894] [nodetwo]: Publishing: "Pong : 9"
[INFO] [1714359821.312417279] [nodetwo]: Subscribing: "Ping : 10"
[INFO] [1714359823.823875657] [nodetwo]: Publishing: "Pong : 10"
```


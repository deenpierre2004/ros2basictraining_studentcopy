import rclpy
from rclpy.node import Node
from std_msgs.msg import String


class NumTwoNode(Node):

    def __init__(self):
        super().__init__('nodetwo')

        #------------------------------------------------
        #                    TODO:
        #  Create your publisher below! Remember that your
        #  publishes string type message to a topic called
        #  second_node
        #------------------------------------------------


        #------------------------------------------------
        #                    TODO:
        #  Create your subscriber below! Remember that your
        #  subscribes to string type message from the topic
        #  first_node
        #------------------------------------------------

        #------------------------------------------------
        #                    TODO:
        #           Create your timer below
        #------------------------------------------------

        self.i = 0

    

    def timer_callback(self):
        msg = String()
        msg.data = 'Pong : %d' % self.i

        #------------------------------------------------
        #                    TODO:
        #  Create a line below that publishes the msg object to
        #  the topic 
        #------------------------------------------------


        #------------------------------------------------
        #                    TODO:
        #  Create a line below which logs what is being publishsed
        #------------------------------------------------
       
        self.i += 1

    

    #------------------------------------------------
    #                    TODO:
    #  Create a callback function for your subscriber
    #  Inside this function create a line that logs
    #  the message

    #------------------------------------------------
        

    


def main(args=None):
    rclpy.init(args=args)
    numtwo_node = NumTwoNode()
    rclpy.spin(numtwo_node)
    numtwo_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
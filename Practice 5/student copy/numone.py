import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class NumOneNode(Node):

    def __init__(self):
        super().__init__('nodeone')
        
        #------------------------------------------------
        #                    TODO:
        #  Create your publisher below! Remember that your
        #  publishes string type message to a topic called
        #  first_node
        #------------------------------------------------


        #------------------------------------------------
        #                    TODO:
        #  Create your subscriber below! Remember that your
        #  subscribes to string type message from the topic
        #  second_node
        #------------------------------------------------

        #------------------------------------------------
        #                    TODO:
        #           Create your timer below
        #------------------------------------------------

        self.i = 0 



    def timer_callback(self):
        msg = String()
        msg.data = 'Ping : %d' % self.i
        #------------------------------------------------
        #                    TODO:
        #  Create a line below that publishes the msg object to
        #  the topic 
        #------------------------------------------------


        #------------------------------------------------
        #                    TODO:
        #  Create a line below which logs what is being publishsed
        #------------------------------------------------

        self.i += 1



    #------------------------------------------------
    #                    TODO:
    #  Create a callback function for your subscriber
    #  Inside this function create a line that logs
    #  the message

    #------------------------------------------------



def main(args=None):
    rclpy.init(args=args)

    numone_node = NumOneNode()

    rclpy.spin(numone_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    numone_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
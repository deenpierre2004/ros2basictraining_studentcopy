# ROS Industrial Consortium Asia Pacific ROS2 Basics Course Final Assessment / ROS Industrial Consortium Asia Pacific ROS2基本コース最終評価

This portion covers the final assessment set-up steps for the ROS2 Basics course. / この部分では、ROS2基本コースの最終評価の設定手順をカバーします

## Overall Objective / 全体的な目標: 

In this final assessment, you will be required to design and implement a ROS2 solution on a physical turtlebot3, which is requried to perform the following functions: / この最終評価では、物理的なturtlebot3上でROS2ソリューションを設計し、実装することが求められます。以下の機能を実行する必要があります:

### Level 1 Functionality / レベル1の機能

1. Upon detection of an aruco marker, the turtlebot should move straight towards the aruco marker. The turtlebot should remain stationary if it does not detect any Aruco markers / arucoマーカーを検出した場合、turtlebotは直進してarucoマーカーに向かうべきです。Arucoマーカーを検出しない場合、turtlebotは静止したままであるべきです


### Level 2 Functionality / レベル2の機能

2. When the robot reaches a predefined distance from the aruco marker, the robot should stop moving towards the marker. / ロボットがarucoマーカーからあらかじめ定義された距離に達したとき、ロボットはマーカーに向かって移動を停止するべきです。


### Level 3 Functionality / レベル3の機能

3. When the robot gets too close, beyond a predefined distance from the aruco marker, instead of stopping, the robot should start reversing. / ロボットがarucoマーカーからあらかじめ定義された距離を超えて近づきすぎた場合、ロボットは停止する代わりに後退を開始するべきです。


### Level 4 Functionality / レベル4の機能
4. If the tag is not directly facing the turtlebot, the turtlebot should rotate to face the tag / タグがturtlebotを直接向いていない場合、turtlebotはタグを向くように回転するべきです


## General Guidelines / 一般的なガイドライン

The Assessment objective can be broken down into a few features / 評価目標はいくつかの機能に分けることができます

1. Aruco Marker Detection / Arucoマーカーの検出
2. Turtlebot Teleoperation Functionality / Turtlebotのテレオペレーション機能
3. Logic flow governing the rules for movement of the robot / ロボットの移動のルールを統治するロジックフロー

Use the `final_assessment.py` file provided that has the code templates for you to fill in. / 提供された`final_assessment.py`ファイルを使用して、コードテンプレートを埋めてください。

### Aruco Marker Detection / Arucoマーカーの検出

In Practical 8, you have already created a ROS2 Server that is able to output the pose of the Aruco Marker. Make use of this service server to aquire information about the pose of the marker. / Practical 8では、すでにArucoマーカーの姿勢を出力できるROS2サーバーを作成しています。このサービスサーバーを使用して、マーカーの姿勢に関する情報を取得してください。

### Turtlebot Teleoperation Functionality / Turtlebotのテレオペレーション機能

In Pactical 3, you have created ROS2 publishers to move the Turtlebot. This should teach you the mechanism needed to move the turtlebot. / Practical 3では、Turtlebotを動かすためのROS2パブリッシャーを作成しました。これにより、turtlebotを動かすためのメカニズムを学ぶことができます。

## Guiding Questions / ガイドラインの質問

1. The system needs to constantly check for an Aruco tag. How can we ensure that the system constantly checks for the tag / システムは常にArucoタグをチェックする必要があります。システムが常にタグをチェックするようにするにはどうすればよいですか？

2. The turtlebot's movement should be reactive based on the information of the location of the tag. How can we only trigger the motion of the robot everytime we detect a tag? This includes not just the distance from the tag, but the angle of the tag to the turtlebot as well. / turtlebotの動きはタグの位置の情報に基づいて反応的であるべきです。タグを検出するたびにロボットの動きをどのようにトリガーすることができますか？これには、タグからの距離だけでなく、タグの角度も含まれます。

3. To prevent the robot from going too fast, we might want to consider making the Turtlebot travel slower the closer it gets to the tag. How do we do this? / ロボットが速すぎてしまわないように、タグに近づくほどTurtlebotが遅く移動するようにすることを考えるかもしれません。これはどのように行いますか？

## Hints / ヒント

### Getting distance of tag from turtlebot / turtlebotからのタグの距離の取得

The linear distance of the tag can simply be found using the Pythagoras theorem as shown in the image below / タグの直線距離は、下の画像に示すようにピタゴラスの定理を使用して簡単に見つけることができます

<img src="./images/pythagoras.png"  width="30%" height="30%">
<br>

In python, it can be represented below / pythonでは、以下のように表現できます

```python
    import math

    def get_tag_distance(self, x, y):
        return math.sqrt((x ** 2) + (y ** 2))
```

### Getting angle of tag from turtlebot / turtlebotからのタグの角度の取得

Similarly, the angle of the tag from the turtlebot can be found by using simple trigonometry / 同様に、タグの角度は簡単な三角法を使用してturtlebotから見つけることができます

<img src="./images/tag_angle.png"  width="30%" height="30%">
<br>

This can be represented in python below / これはpythonでは以下のように表現できます:

```python
    import numpy as np

    def get_tag_angle(self, x, y):
        return np.arctan(y/x)
```


<!-- 
## Final Assessment 1.1 - Detecting AR Tag ID and Pose Information
Making reference the previous days Exercises 3 & 4, you are now required to write a new Service Client to capture the detected Aruco Marker Pose.

1. Collect an **ARUCO Marker** from the trainers.
2. Within the `final_assessment` package, make use of the script `aruco_tracker.py` , follow the instructions within the file to create a service client to capture the detected pose of the Aruco Marker.
3. Remember to add the entry points for your executables in your `setup.py`.

## Final Assessment 1.2 - Publishing Commands to the Turtlebot3
Making reference to the previous days Exercise 2, you are now required to publish commands to get the Turtlebot3 to move, in combination with the information gathered from the previous step 1.1.

1. Within the `final_assessment` package, make use of the script `teleop_aruco.py` and follow the instructions within the file to publish commands to move the turtlebot based on the pose information.

## Final Assessment 1.3 - Timer Callback to for Pose and Update Position of Turtlebot
1. Within the `final_assessment` package, include a `timer and callback` function in the  `aruco_tracker.py` script to constantly request for the pose of the marker and update the position of the turtlebot3.

## Final Assessment 1.4 - Writing a Launch File to Test your Answers
Making reference to the previous days Exercise 5, you are required to write a launch file to consolidate the multiple nodes to launch everything required for this final assessment.

1. Create a `launch` folder within the `final_assessment` package.
2. Create a launch file named `final_assessment.launch.py` that contains the multiple nodes required to launch everything.

## Verifying your Solution
Move the aruco marker back and forth as well as sideways and observe if the turtlebot moves towards it and faces in the same direction as it.

If the turtlebot3 behaves as intended, congratulations! You have now completed the final assessment for the ROS2 Basics course. -->
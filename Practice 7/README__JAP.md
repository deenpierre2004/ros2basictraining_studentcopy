# Introduction to Practice 7: ROS 2 Subscriber with complex message / 複雑なメッセージを持つROS 2サブスクライバー

## Learning outcomes / 学習成果
In this practice it will test learners on their understanding of ROS 2 Subscriber with complex message. / この実習では、複雑なメッセージを持つROS 2サブスクライバーについての理解を学習者にテストします。

## Practice Objective / 実習目標

The user will be able to create their own ROS 2 Subscriber with complex message upon completion of this practice. / この実習を完了すると、ユーザーは自分自身のROS 2サブスクライバーを複雑なメッセージで作成することができます。

## Prerequsite / 前提条件
Ensure that you have completed Practical 6 already. / 実践 6 がすでに完了していることを確認してください。


## Step 1 : Creating ROS 2 Subscriber / ROS 2サブスクライバーの作成

**Technical Requirements / 技術要件**

1. Within the same `training_ws` where you have done your previous pratical at, go into the src folder / 以前の実習を行った同じ`training_ws`内で、srcフォルダに入ってくだっさい。

2. Create a ROS 2 python package named `complex_subscriber` within the src folder / srcフォルダ内に`complex_subscriber`という名前のROS 2 pythonパッケージを作成してください。

3. Create a ROS 2 excutable in this package named `pratical7_node` / このパッケージに`pratical7_node`という名前のROS 2実行可能ファイルを作成してください

4. You may use the provided code boiler plate `my_subscriber.py` as a starting point / 
提供されたコードボイラープレート`my_subscriber.py`を開始点として使用することができます。

    - Navigate to the `training_ws/src/complex_subscriber/complex_subscriber` directory and store the `my_subscriber.py` file there / `training_ws/src/complex_subscriber/complex_subscriber`ディレクトリに移動し、`my_subscriber.py` ファイルをそこに保存してください。


5. Inside the `my_subscriber` file , implement the following task. / `my_subscriber` ファイル内で、以下のタスクを実装してください。


    - Create a subscriber node that listens to the SimpleVelocity messages that is published on the `simple_vel` topic. / `simple_vel`トピックで公開されているSimpleVelocityメッセージをリッスンするサブスクライバーノードを作成してください。
    
    - Create a callback function that logs the linear and angular velocity from the received messages. / 受信したメッセージから線形速度と角速度をログに記録するコールバック関数を作成してください。
    
    - Remeber to import your SimpleVelocity message in order to run this pratical / この実習を実行するためには、SimpleVelocityメッセージをインポートすることを忘れないでください

    - #### Use the comments in the provided code boiler plate as a guideline / 提供されたコードボイラープレートのコメントをガイドラインとして使用してください

6. Inside your `package.xml` file, include the following dependecies within the <package> element of `package.xml` file / `package.xml`ファイル内で、`package.xml`ファイルの<package>要素内に以下の依存関係を含めまてください

```
 <exec_depend>simplevelocity_msg</exec_depend>
```

7. Make sure that all the files are saved prior to building the `complex_subscriber` package. / すべてのファイルが保存されていることを確認してから、`complex_subscriber`パッケージをビルドしてください。

## Step 2 : Verifying your solution / 解答の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソース化することを忘れないでください

### 1. Bringup and initialise the turtlebot /  Turtlebotの起動と初期化

Check that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT**. / TURTLEBOTと同じネットワークに接続していることを確認してください。

In a new terminal, run the following commands / 新しいターミナルで、以下のコマンドを実行してください:

```bash
ssh ubuntu@<ip_address>

ros2 launch turtlebot3_bringup robot.launch.py 

```
Once you see the following output from the terminal, it means that you have successfully bringup the turtlebot. / ターミナルから以下の出力が見えたら、それはあなたがTurtlebotを正常に起動したことを意味します

```bash
[INFO] [launch]: All log files can be found below /home/ubuntu/.ros/log/2019-08-19-01-24-19-009803-ubuntu-15310
[INFO] [launch]: Default logging verbosity is set to INFO
urdf_file_name : turtlebot3_burger.urdf
[INFO] [robot_state_publisher-1]: process started with pid [15320]
[INFO] [hlds_laser_publisher-2]: process started with pid [15321]
[INFO] [turtlebot3_ros-3]: process started with pid [15322]
[robot_state_publisher-1] Initialize urdf model from file: /home/ubuntu/turtlebot_ws/install/turtlebot3_description/share/turtlebot3_description/urdf/turtlebot3_burger.urdf
[robot_state_publisher-1] Parsing robot urdf xml string.
[robot_state_publisher-1] Link base_link had 5 children
[robot_state_publisher-1] Link caster_back_link had 0 children
[robot_state_publisher-1] Link imu_link had 0 children
[robot_state_publisher-1] Link base_scan had 0 children
[robot_state_publisher-1] Link wheel_left_link had 0 children
[robot_state_publisher-1] Link wheel_right_link had 0 children
[robot_state_publisher-1] got segment base_footprint
[robot_state_publisher-1] got segment base_link
[robot_state_publisher-1] got segment base_scan
[robot_state_publisher-1] got segment caster_back_link
[robot_state_publisher-1] got segment imu_link
[robot_state_publisher-1] got segment wheel_left_link
[robot_state_publisher-1] got segment wheel_right_link
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Init TurtleBot3 Node Main
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Init DynamixelSDKWrapper
[turtlebot3_ros-3] [INFO] [DynamixelSDKWrapper]: Succeeded to open the port(/dev/ttyACM0)!
[turtlebot3_ros-3] [INFO] [DynamixelSDKWrapper]: Succeeded to change the baudrate!
[robot_state_publisher-1] Adding fixed segment from base_footprint to base_link
[robot_state_publisher-1] Adding fixed segment from base_link to caster_back_link
[robot_state_publisher-1] Adding fixed segment from base_link to imu_link
[robot_state_publisher-1] Adding fixed segment from base_link to base_scan
[robot_state_publisher-1] Adding moving segment from base_link to wheel_left_link
[robot_state_publisher-1] Adding moving segment from base_link to wheel_right_link
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Start Calibration of Gyro
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Calibration End
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Motors
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Wheels
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Sensors
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create battery state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create imu publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create sensor state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create joint state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Devices
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create motor power server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create reset server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create sound server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Run!
[turtlebot3_ros-3] [INFO] [diff_drive_controller]: Init Odometry
[turtlebot3_ros-3] [INFO] [diff_drive_controller]: Run!
```

To check if your solution works, go into a new terminal and run the following commands / あなたの解答が機能するかどうかを確認するために、新しいターミナルを開き、以下のコマンドを実行してください:

### 2. Run your Practical 6 Node / 実践的な 6 ノードを実行する

``` linux
cd training_ws
source install/setup.bash
ros2 run complex_publisher praｃtical6_node
```

### 3. Run your Practical 7 node / 実践的な 7 ノードを実行する

``` linux
cd training_ws
source install/setup.bash
ros2 run complex_subscriber praｃtical7_node
```

Once you see the following output from the terminal, it means that you have successfully completed this practical. / ターミナルに次の出力が表示されたら、この実習は正常に完了しました。

```
[INFO] [1714115107.187829907] [MySubscriber]: Received linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714115108.183861835] [MySubscriber]: Received linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714115109.183653317] [MySubscriber]: Received linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714115110.183785031] [MySubscriber]: Received linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
[INFO] [1714115111.183750838] [MySubscriber]: Received linear velocity: "0.15" m/s, angular velocity: "0.9999999999999999" rad/s
```

# Introduction to ROS 2 Practical 10 / ROS 2 実習10への導入

## Learning outcomes / 学習成果

This practical will test learners on their understanding of ROS 2 Launch Files / この実習では、学習者がROS 2ランチファイルについて理解しているかをテストします。

## Exercise Objective / 実習の目的

- In this practical, you will need to be able to create a package with a ROS 2 launch files to launch every node in Practical 9. / この実習では、実習9のすべてのノードを起動するROS 2ランチファイルを含むパッケージを作成できるようになる必要があります。
- This means that you should only need **one terminal** to run everything / これは、すべてを実行するために1つのターミナルだけが必要であることを意味します。

## Technical requirements / 技術要件

1. Create a launch folder in the `object_spawner` package / `object_spawner` パッケージにランチフォルダを作成してください。
2. Create a launch file, `demo.launch.py` in the `launch` folder / `launch`フォルダに`demo.launch.py`という名前のランチファイルを作成してください

    You may use the provided `demo.launch.py` as a starting point for your code. This file contains / 提供された`demo.launch.py`をコードの出発点として使用することができます。このファイルには以下が含まれています:

    1. All libraries to be imported for the launch file / ランチファイルのためのすべてのライブラリをインポートします
    
    2. Instructions for all the nodes that need to be created to be launched / 起動する必要があるすべてのノードの指示を作成します

	3. A helper function `generate_launch_description` that will return your `LaunchDescription` object / `LaunchDescription`オブジェクトを返すヘルパー関数`generate_launch_description`を作成します。

3. Copy over the `practical_9.rviz` file to the same `launch` folder. / `practical_9.rviz` ファイルを同じ`launch`フォルダにコピーしてください
	- The RViz file is a configuration file that allows you to launch pre-defined settings. This means you dont need to add the Marker 	Plugin to the RViz GUI for exercise 4 everytime to run the demo / RVizファイルは、事前に定義された設定を起動できる設定ファイルです。これは、デモを実行するたびにRViz GUIにマーカープラグインを追加する必要がないことを意味します

4. Populate the launch file with the following / 以下の内容でランチファイルを埋めます

	- Add a `Node` for the aruco detection node to the `launch_description` / `launch_description`にaruco検出ノードのための`Node`を追加してください。

		- Package Name: Refer to Pratical 8B
		- Executable Name: Refer to Pratical 8B
		- Node Name: `"aruco_detection_node"`
		- Ouput: log

	- Add a `Node` for the object spawner node to the `launch_description` / `launch_description`にobject spawnerノードのための`Node`を追加してください
		- Package Name: Refer to Practical 9A
		- Executable Name: Refer to Practical 9A
		- Node Name: `"object_spawner_node"`
		- Ouput: log


	- Create a variable `rviz_config_dir ` using the `os.path.join` and `get_package_share_directory` functions / `os.path.join`と`get_package_share_directory`関数を使用して`rviz_config_dir`という変数を作成してください

	- Add a `Node` for the rviz2 node to the `launch_description` / `launch_description`にrviz2ノードのための`Node`を追加してください
    
		- Package Name: `"rviz2"`
		- Executable Name: `"rviz2"`
		- Node Name: `"rviz2_node"`
		- Ouput: log
		- Arguments: `rviz_config_dir`


5. Edit your `setup.py` file to install the launch and rviz files / `setup.py` ファイルを編集して、launchファイルとrvizファイルをインストールしてください
	- Remember to add the following libraries into the `setup.py` file / `setup.py` ファイルに以下のライブラリを追加することを忘れないでください

	```python
	import os
	from glob import glob
	from setuptools import setup, find_packages
	```
	- As a hint, you will be able to install the `practical_9.rviz` file by adding this line in the `data_files` array in your `setup.py` file / ヒントとして、`setup.py` ファイルの`data_files`配列に以下の行を追加することで、`practical_9.rviz` ファイルをインストールできます
	```python
		(os.path.join('share', package_name,'launch'), glob('launch/*.rviz'))
	```
	- Do the same for the launch file in the `launch` folder. / `launch`フォルダのlaunchファイルについても同様に行ってください

## Verifying your solution / 解決策の確認

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に、ワークスペースをビルドしてソース化することを忘れないでください

To check if your solution works, run the following / 解決策が機能するかどうかを確認するには、以下を実行してください

### 1. Run the camera driver on the turtlebot3 / turtlebot3上でカメラドライバを実行します

Before running the camera driver, ensure that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** / カメラドライバを実行する前に、TURTLEBOTと同じネットワークに接続し、環境に`ROS_DOMAIN_ID`をエクスポートしていることを確認してください

```bash
ssh ubuntu@<ip_address>

ros2 launch usb_cam demo_launch.py

```

### 2. Run your demo /  デモを実行します

In another terminal within your VM, ensure that you have **SOURCED YOUR ROS DISTRO AND YOUR WORKSPACE AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT**, then run the following command / VM内の別のターミナルで、ROS DISTROとワークスペースをソース化し、環境に`ROS_DOMAIN_ID`をエクスポートしていることを確認した後、以下のコマンドを実行してください:

```bash
ros2 launch object_spawner demo.launch.py

```
With this one launch file, you should launch everything that you created in Practical 9 / この一つのlaunchファイルで、Practical 9 で作成したすべてのものを起動する必要があります。

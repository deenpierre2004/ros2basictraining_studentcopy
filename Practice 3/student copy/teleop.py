import os
import select
import sys

if os.name == 'nt':
    import msvcrt
else:
    import termios
    import tty

import rclpy
from rclpy.node import Node
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor

msg = """
Control your TurtleBot3!
---------------------------
Moving around:
        w    
   a    s    d
        x    

w/x : increase/decrease linear velocity (Burger : ~ 0.22, Waffle and Waffle Pi : ~ 0.26)
a/d : increase/decrease angular velocity (Burger : ~ 2.84, Waffle and Waffle Pi : ~ 1.82)
s : stop

CTRL-C to quit
"""

def get_key(settings):
    if os.name == 'nt':
        return msvcrt.getch().decode('utf-8')
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

class TeleopKeyboard(Node):
    def __init__(self):
        # node name 'keyboard_input'
        super().__init__('teleop_keyboard')
        # print Initialize object
        self.get_logger().info("Initialize object")

        #------------------------------------------------
        #                    TODO:
        #  Create your publisher here! Remember to check
        #  what is the publisher type you need.
        #  Once you have completed creating your publisher,
        #  Initialise a twist message and call it self.cmd_msg
        #------------------------------------------------

        #-------- A Timer is created here every 0.1s, the timer_callback() function will be called. ---------
        timer_period = 0.1
        client_cb_group = MutuallyExclusiveCallbackGroup()
        self.timer = self.create_timer(timer_period, self.timer_callback, callback_group=client_cb_group)
        print(msg)

    def timer_callback(self):
        settings = None
        if os.name != 'nt':
            settings = termios.tcgetattr(sys.stdin)

        self.key = get_key(settings)

        if self.key == 'w':                                                         
            self.get_logger().info("Moving forwards")
            self.cmd.linear.x += 0.1
            # self.cmd.angular.z = 0.0
            self.get_logger().info("currently:\tlinear velocity %s\tangular velocity %s" % (self.cmd.linear.x, self.cmd.angular.z))
            
        elif self.key == 'a':                                                         
            self.get_logger().info("Turning left")
            # self.cmd.linear.x = 0.0
            self.cmd.angular.z += 0.1
            self.get_logger().info("currently:\tlinear velocity%s\tangular velocity %s" % (self.cmd.linear.x, self.cmd.angular.z))
            
        elif self.key == 'x':                                                         
            self.get_logger().info("Moving backwards")
            self.cmd.linear.x -= 0.1
            # self.cmd.angular.z = 0.0
            self.get_logger().info("currently:\tlinear velocity %s\tangular velocity %s" % (self.cmd.linear.x, self.cmd.angular.z))

        
        #-------------------------------------------------
        #                    TODO:
        #          Write your code here!
        #          Now you have the keyboard input from the 
        #          User, what do you do with it?
        #-------------------------------------------------

    
def main(args=None):
    # initialize the ROS2 communication
    rclpy.init(args=args)
    keyboard_input = TeleopKeyboard()    
    executor = MultiThreadedExecutor()
    executor.add_node(keyboard_input)
    executor.spin()
    keyboard_input.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
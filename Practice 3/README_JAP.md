# Introduction to Practice 3: Create your own ROS 2 Publisher Node / 自分だけのROS 2パブリッシャーノードを作成します。

## Learning outcomes / 学習成果

This exercise will test learners on their understanding of Publishers. / この演習では、パブリッシャーについての理解を学習者がテストします. 

## Exercise Objective

In this exercise, you will need to be able to create a package that allows you to control a simulated turtle bot with your keyboard. / この演習では、キーボードでシミュレートされたTurtleBotを制御できるパッケージを作成する必要があります。

The method to control the turtlebot should behave as follows / TurtleBotを制御する方法は、次のように動作する必要があります:

```bash
Control your TurtleBot3!
---------------------------
Moving around:
        w    
   a    s    d
        x    

w/x : increase/decrease linear velocity 
a/d : increase/decrease angular velocity 
s : stop

```

You will be using a **ROS2 Publisher** to publish some messages to the topic named `/cmd_vel`, which is used to control the actual turtlebot hardware. / **ROS2 Publisher** を使用して、`/cmd_vel` というトピックにメッセージを公開します。これは、実際のTurtleBotハードウェアを制御するために使用されます。

## Technical requirements

1. Create a workspace named `training_ws` / `training_ws` という名前のワークスペースを作成してください。

2. Within the `training_ws` folder, create a `src` folder / `training_ws` フォルダ内に` src` フォルダを作成してください。

3. Create a ROS 2 python package named `teleop_keyboard` / `teleop_keyboard`という名前のROS 2 pythonパッケージを作成してください。

4. Create a ROS 2 executable in this package named `teleop_node` / このパッケージに`teleop_node`という名前のROS 2実行可能ファイルを作成してください。

    - You may use the provided code boiler plate `teleop.py` as a starting point / 提供されたコードのひな形 teleop.pyを出発点として使用することができます。

    - Navigate to the `training_ws/src/teleop_keyboard/teleop_keyboard` directory and store the `teleop.py` file there. / `training_ws/src/teleop_keyboard/teleop_keyboard` ディレクトリに移動し、そこに `teleop.py` ファイルを保存してください。

    - Inside your `package.xml` add the following dependecies within the <package> element of the file. / `package.xml` 内で、ファイルの <package> 要素内に次の依存関係を追加します。
    ```
    <depend>rclpy</depend>
    <depend>geometry_msgs</depend>
    ```


5. Create a node that is able to read the user keyboard input, and publishes the correct commands to the `/cmd_vel` topic / ユーザーのキーボード入力を読み取り、`/cmd_vel` トピックに正しいコマンドを公開するノードを作成してください。

6. To read the keystrokes of the user, you may use the following helper function / ユーザーのキーストロークを読み取るために、次のヘルパー関数を使用することができます:

```python
def get_key(settings):
    if os.name == 'nt':
        return msvcrt.getch().decode('utf-8')
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key
```

To use this `get_key()` function, the following is a code snippet of how you can use it.

```python
    settings = None
    if os.name != 'nt':
        settings = termios.tcgetattr(sys.stdin)
    user_key = get_key(settings)
```

## Verifying your solution / あなたの解決策を検証します

Before doing this. **REMEMBER TO BUILD AND SOURCE YOUR WORKSPACE** / これを行う前に. **ワークスペースをビルドしてソースにすることを忘れないでください**

To check if your solution works, run the following / あなたの解決策が機能するかどうかを確認するために、次の操作を実行してください

<!-- ### 1. Run the turtlebot simulation

```bash
export TURTLEBOT3_MODEL=burger
ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
``` -->

### 1. Bringup and initialise the turtlebot / Turtlebotを起動し、初期化します

Check that you have **CONNECTED TO THE SAME NETWORK AS YOUR TURTLEBOT**. / あなたがTURTLEBOTと同じネットワークに接続していることを確認してください

In a new terminal, run the following commands / 新しいターミナルで、次のコマンドを実行してください :

```bash
ssh ubuntu@<ip_address>

ros2 launch turtlebot3_bringup robot.launch.py 

```
Once you see the following output from the terminal, it means that you have successfully bringup the turtlebot. / ターミナルから次の出力が表示されたら、Turtlebotの起動に成功したことを意味します.

```bash
[INFO] [launch]: All log files can be found below /home/ubuntu/.ros/log/2019-08-19-01-24-19-009803-ubuntu-15310
[INFO] [launch]: Default logging verbosity is set to INFO
urdf_file_name : turtlebot3_burger.urdf
[INFO] [robot_state_publisher-1]: process started with pid [15320]
[INFO] [hlds_laser_publisher-2]: process started with pid [15321]
[INFO] [turtlebot3_ros-3]: process started with pid [15322]
[robot_state_publisher-1] Initialize urdf model from file: /home/ubuntu/turtlebot_ws/install/turtlebot3_description/share/turtlebot3_description/urdf/turtlebot3_burger.urdf
[robot_state_publisher-1] Parsing robot urdf xml string.
[robot_state_publisher-1] Link base_link had 5 children
[robot_state_publisher-1] Link caster_back_link had 0 children
[robot_state_publisher-1] Link imu_link had 0 children
[robot_state_publisher-1] Link base_scan had 0 children
[robot_state_publisher-1] Link wheel_left_link had 0 children
[robot_state_publisher-1] Link wheel_right_link had 0 children
[robot_state_publisher-1] got segment base_footprint
[robot_state_publisher-1] got segment base_link
[robot_state_publisher-1] got segment base_scan
[robot_state_publisher-1] got segment caster_back_link
[robot_state_publisher-1] got segment imu_link
[robot_state_publisher-1] got segment wheel_left_link
[robot_state_publisher-1] got segment wheel_right_link
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Init TurtleBot3 Node Main
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Init DynamixelSDKWrapper
[turtlebot3_ros-3] [INFO] [DynamixelSDKWrapper]: Succeeded to open the port(/dev/ttyACM0)!
[turtlebot3_ros-3] [INFO] [DynamixelSDKWrapper]: Succeeded to change the baudrate!
[robot_state_publisher-1] Adding fixed segment from base_footprint to base_link
[robot_state_publisher-1] Adding fixed segment from base_link to caster_back_link
[robot_state_publisher-1] Adding fixed segment from base_link to imu_link
[robot_state_publisher-1] Adding fixed segment from base_link to base_scan
[robot_state_publisher-1] Adding moving segment from base_link to wheel_left_link
[robot_state_publisher-1] Adding moving segment from base_link to wheel_right_link
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Start Calibration of Gyro
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Calibration End
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Motors
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Wheels
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Sensors
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create battery state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create imu publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create sensor state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create joint state publisher
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Add Devices
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create motor power server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create reset server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Succeeded to create sound server
[turtlebot3_ros-3] [INFO] [turtlebot3_node]: Run!
[turtlebot3_ros-3] [INFO] [diff_drive_controller]: Init Odometry
[turtlebot3_ros-3] [INFO] [diff_drive_controller]: Run!
```

### 2. Run your teleop keyboard node / Teleopキーボードノードを実行します

In a new terminal within your VM,  ensure that you have **SOURCED YOUR ROS DISTRO AND EXPORTED THE `ROS_DOMAIN_ID` IN YOUR ENVIRONMENT** before running the following command / VM 内の新しいターミナルで、次のコマンドを実行する前に、ROS distroをソースにして、環境内でROS_DOMAIN_IDをエクスポートしたことを確認してください:

```bash
ros2 run teleop_keyboard teleop_node

```
If you have input all the necessary keys for the teleop control, you should see the following output on the terminal / Teleopコントロールのための必要なすべてのキーを入力した場合、ターミナルに次の出力が表示されるはずです:

```bash
Control your TurtleBot3!
---------------------------
Moving around:
        w    
   a    s    d
        x    

w/x : increase/decrease linear velocity 
a/d : increase/decrease angular velocity 
s : stop

```

Now try to control your robot using your keyboard. / キーボードを使ってロボットを操作してみてください

## Hints

1. First run the turtlebot simulation. What is the message type for this topic? / まず、turtlebotのシミュレーションを実行します。このトピックのメッセージタイプは何ですか? 

2. What is the structure of this ROS2 interface? / このROS2インターフェースの構造は何ですか

## Troubleshooting / トラブルシューティング

1. When Running the turtlebot demo, you may receive this error. / turtlebotのデモを実行すると、このエラーが表示されることがあります

```bash
[spawn_entity.py-4] [INFO] [1683014552.804862846] [spawn_entity]: Waiting for service /spawn_entity, timeout = 30
[spawn_entity.py-4] [INFO] [1683014552.805078957] [spawn_entity]: Waiting for service /spawn_entity
[spawn_entity.py-4] [ERROR] [1683014582.860819301] [spawn_entity]: Service %s/spawn_entity unavailable. Was Gazebo started with GazeboRosFactory?
[spawn_entity.py-4] [ERROR] [1683014582.861927448] [spawn_entity]: Spawn service failed. Exiting.
```

Solution / 解決策:

Include the following in the terminal before running the turtlebot demo / Turtlebotのデモを実行する前に、ターミナルに以下を含めてください

```bash
gazebo -s libgazebo_ros_init.so -s libgazebo_ros_factory.so myworld.world
```
2. When Running the turtlebot demo, you may receive this error. / Turtlebotのデモを実行すると、このエラーが表示されることがあります

```bash
[spawn_entity.py-4] [INFO] [1683016201.515322905] [spawn_entity]: Calling service /spawn_entity
[spawn_entity.py-4] [INFO] [1683016201.538668263] [spawn_entity]: Spawn status: Entity [burger] already exists.
[spawn_entity.py-4] [ERROR] [1683016201.538968440] [spawn_entity]: Spawn service failed. Exiting.

```

Solution: An instance of the gazebo server is still running somewhere. We need to kill it / Gazeboサーバーのインスタンスがどこかでまだ実行されています。それを終了する必要があります
```bash
killall -e gzserver
```

